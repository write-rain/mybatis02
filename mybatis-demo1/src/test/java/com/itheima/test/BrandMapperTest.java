package com.itheima.test;

import com.itheima.pojo.Brand;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrandMapperTest {
    SqlSessionFactory sqlSessionFactory;

    @Before
    public void before() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void testSelectAll() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        List<Brand> brands = sqlSession.selectList("com.itheima.mapper.BrandMapper.selectAll1");
        System.out.println(brands);
        //关闭资源
        sqlSession.close();
    }


    @Test
    public void testSelectById() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        Brand brand = sqlSession.selectOne("com.itheima.mapper.BrandMapper.selectById", 2);
        System.out.println("brand = " + brand);
        //释放资源
        sqlSession.close();
    }


    @Test
    public void testSelectByCondition() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //接收参数
        String companyName = "华为";

        //处理参数
        companyName = "%" + companyName + "%";

        //封装对象
        Map map = new HashMap();
        map.put("companyName", companyName);

        List<Brand> brands = sqlSession.selectList("com.itheima.mapper.BrandMapper.selectByCondition", map);
        System.out.println(brands);
        //释放资源
        sqlSession.close();

    }


    @Test
    public void testSelectByConditionSingle() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //接收参数
        String companyName = "华为";

        //处理参数
        companyName = "%" + companyName + "%";

        //封装对象
        Brand brand = new Brand();
        brand.setCompanyName(companyName);

        List<Brand> brands = sqlSession.selectList("com.itheima.mapper.BrandMapper.selectByCondition", brand);
        System.out.println(brands);

        //释放资源
        sqlSession.close();

    }


    @Test
    public void testInsert() throws IOException {
        //开启事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        Brand brand = new Brand(null, "波导", "波导手机", 100, "手机中的战斗机", 1);

        sqlSession.insert("com.itheima.mapper.BrandMapper.insert", brand);
        //提交事务
        sqlSession.commit();
        //释放资源
        sqlSession.close();
    }


    @Test
    public void testUpdate() throws IOException {
        //开启事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //接收参数
        Brand brand = new Brand(3, null, null, null, null, 0);
        sqlSession.update("com.itheima.mapper.BrandMapper.update", brand);

        //提交事务
        sqlSession.commit();
        //释放资源
        sqlSession.close();

    }


    @Test
    public void testDeleteById() throws IOException {
        //开启事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //接收参数
        int id = 4;
        sqlSession.delete("com.itheima.mapper.BrandMapper.deleteById", id);

        //提交事务
        sqlSession.commit();

        //释放资源
        sqlSession.close();
    }


    @Test
    public void testDeleteByIds() throws IOException {
        //开启事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //接收参数
        int[] ids = {3, 4};
        sqlSession.delete("com.itheima.mapper.BrandMapper.deleteByIds", ids);
        //提交事务
        sqlSession.commit();
        //释放资源
        sqlSession.close();
    }


}
