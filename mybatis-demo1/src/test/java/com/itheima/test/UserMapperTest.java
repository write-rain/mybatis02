package com.itheima.test;

import com.itheima.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Author itheima
 * @Date 2022/4/1 11:54
 */
public class UserMapperTest {

    SqlSession sqlSession;

    @Before
    public void before() throws IOException {
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
    }

    @Test
    public void selectAll() {
        List<User> users = sqlSession.selectList("com.itheima.mapper.UserMapper.selectAll");
        users.forEach(u -> {
            System.out.println(u);
        });
    }

    @Test
    public void selectAll2() {
        User user1 = new User();
        user1.setUsername("李四");
        user1.setPassword("234");
        List<User> users = sqlSession.selectList("com.itheima.mapper.UserMapper.selectBy", user1);
        users.forEach(u -> {
            System.out.println(u);
        });
    }

}
