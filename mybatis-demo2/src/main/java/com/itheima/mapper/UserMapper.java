package com.itheima.mapper;

import com.itheima.pojo.User;

import java.util.List;

public interface UserMapper {
    /**
     * @TODO 1.查询所有用户, 完成映射文件标签书写
     */
    List<User> selectAll();

    /**
     * @TODO 2.根据主键查询用户, 完成映射文件标签书写
     */
    User selectById(int id);

    /**
     * @TODO 3.根据如下2个条件查询用户，继续完善接口方法，完成映射文件标签书写
     */
    User select(String username, String password);
}
