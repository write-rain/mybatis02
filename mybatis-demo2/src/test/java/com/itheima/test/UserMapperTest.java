package com.itheima.test;

import com.itheima.mapper.UserMapper;
import com.itheima.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * (1)代理模式优势：
 * 1.分业务来处理数据操作
 * 2.代码更简洁，符合面向接口编程
 * <p>
 * (2)代理模式满足条件
 * 1.接口的全限定名称和映射文件的命名空间名要一致
 * 2.接口方法名和映射文件中标签id名要一致
 * 3.接口方法中参数类型和映射文件标签parameterType要一致
 * 4.接口方法返回值类型和映射文件标签resultType和resultMap要一致
 * 注意：接口方法中
 */
public class UserMapperTest {
    SqlSession sqlSession;
    UserMapper userMapper;


    @Before
    public void before() throws Exception {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        sqlSession = sqlSessionFactory.openSession();
        //3. 获取Mapper接口的代理对象
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    /**
     * @TODO 1.测试查询所有用户
     */
    @Test
    public void testSelectAll() throws IOException {
        //1. 获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2. 获取SqlSession对象
        sqlSession = sqlSessionFactory.openSession();
        //3. 获取Mapper接口的代理对象
        userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = sqlSession.selectList("com.itheima.mapper.UserMapper.selectAll");
        users.forEach(u -> {
            System.out.println(u);
        });
        sqlSession.close();
    }


    /**
     * @TODO 2.测试根据主键查询用户
     */
    @Test
    public void testSelectById() throws IOException {

//        1.获取sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//        2.获取SqlSession对象
        sqlSession = sqlSessionFactory.openSession();
//        3.获取mapper接口的代理对象
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.selectById(2);
        System.out.println("user = " + user);

    }

    /**
     * @TODO 3.测试根据如下2个条件查询用户
     * username："赵六"
     * password: "123"
     */
    @Test
    public void testSelect() throws IOException {
//        1.获取sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//        2.获取SqlSession对象
        sqlSession = sqlSessionFactory.openSession();
//

        User users = new User();

        users.setUsername("赵六");
        users.setPassword("123");
        List<User> objects = sqlSession.selectList("com.itheima.mapper.UserMapper.select", users);
        objects.forEach(u -> {
            System.out.println("u = " + u);
      
        });


    }


    @After
    public void after() throws Exception {
        sqlSession.close();
    }
}
