package com.itheima.mapper;

import com.itheima.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {
    /**
     * @TODO 1.在以下接口方法上完成查询所有用户sql语句
     */
    List<User> selectAll();

    /**
     * @TODO 2.在以下接口方法上完成根据主键查询用户sql语句
     */
    User selectById(int id);

    /**
     * @TODO 3.在以下接口方法上完成根据用户名和密码查询用户sql语句，完善方法参数书写！！！
     */
    User selectByCondition(String username, String password);

    /**
     * @TODO 4.测试以下接口方法定义是否正确？
     */
    @Select("select * from user where name=#{username}")
    public User selectByCondition(String name);
}
