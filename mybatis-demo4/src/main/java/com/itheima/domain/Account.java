package com.itheima.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 账号
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 关联的用户id
     */
    private Integer uid;
    /**
     * 金额
     */
    private Double money;

    //从表实体应该包含一个主表实体的对象引用
    private User user;

}
