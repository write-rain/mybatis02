package com.itheima.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountUser extends Account {
    /**
     * 账户
     */
    private String username;
    /**
     * 地址
     */
    private String address;

}
