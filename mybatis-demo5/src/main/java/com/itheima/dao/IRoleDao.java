package com.itheima.dao;

import com.itheima.domain.Role;

import java.util.List;

public interface IRoleDao {

    /**
     * 查询所有角色，同时获取到角色下所有用户的信息
     *
     * @return
     */
    List<Role> findAll();
}
