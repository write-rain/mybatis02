create database 2021_spring_db;
use 2021_spring_db;
-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: 2021_mybatis_db
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account`
(
    `id`    int(11) NOT NULL AUTO_INCREMENT COMMENT '账户id',
    `uid`   int(11) NOT NULL COMMENT '外键，关联用户主键id',
    `money` double  NOT NULL COMMENT '账户金额',
    PRIMARY KEY (`id`),
    UNIQUE KEY `account_id_uindex` (`id`),
    KEY `account_user_id_fk` (`uid`),
    CONSTRAINT `account_user_id_fk` FOREIGN KEY (`uid`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account`
    DISABLE KEYS */;
INSERT INTO `account` (`id`, `uid`, `money`)
VALUES (1, 3, 1000),
       (2, 3, 500),
       (3, 3, 2000),
       (4, 1, 5000),
       (5, 1, 600),
       (6, 2, 1000),
       (7, 2, 500),
       (8, 2, 4000),
       (9, 2, 6000);
/*!40000 ALTER TABLE `account`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role`
(
    `roleId`   int(11) NOT NULL AUTO_INCREMENT COMMENT '角色主键id',
    `roleName` varchar(30) DEFAULT NULL COMMENT '角色名称',
    `releDesc` varchar(30) DEFAULT NULL COMMENT '角色描述 ',
    PRIMARY KEY (`roleId`),
    UNIQUE KEY `role_roleId_uindex` (`roleId`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8 COMMENT ='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role`
    DISABLE KEYS */;
INSERT INTO `role` (`roleId`, `roleName`, `releDesc`)
VALUES (1, 'admin', '管理员'),
       (2, 'user', '普通用户');
/*!40000 ALTER TABLE `role`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_brand`
--

DROP TABLE IF EXISTS `tb_brand`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_brand`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `brand_name`   varchar(20)  DEFAULT NULL,
    `company_name` varchar(20)  DEFAULT NULL,
    `ordered`      int(11)      DEFAULT NULL,
    `description`  varchar(100) DEFAULT NULL,
    `status`       int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_brand`
--

LOCK TABLES `tb_brand` WRITE;
/*!40000 ALTER TABLE `tb_brand`
    DISABLE KEYS */;
INSERT INTO `tb_brand` (`id`, `brand_name`, `company_name`, `ordered`, `description`, `status`)
VALUES (1, '三只松鼠', '三只松鼠股份有限公司', 5, '好吃不上火', 0),
       (2, '华为', '华为技术有限公司', 100, '华为致力于把数字世界带入每个人、每个家庭、每个组织，构建万物互联的智能世界', 1),
       (3, '小米', '小米科技有限公司', 50, 'are you ok', 0);
/*!40000 ALTER TABLE `tb_brand`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(20) DEFAULT NULL,
    `password` varchar(20) DEFAULT NULL,
    `gender`   char(1)     DEFAULT NULL,
    `address`  varchar(30) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user`
    DISABLE KEYS */;
INSERT INTO `tb_user` (`id`, `username`, `password`, `gender`, `address`)
VALUES (1, '赵六', '123', '男', '北京'),
       (2, '李四', '234', '女', '天津'),
       (3, '王五', '111', '男', '西安');
/*!40000 ALTER TABLE `tb_user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user`
(
    `id`       int(11)     NOT NULL AUTO_INCREMENT COMMENT '用户id',
    `username` varchar(30) NOT NULL COMMENT '用户名',
    `address`  varchar(30) DEFAULT NULL COMMENT '地址',
    `sex`      int(11)     DEFAULT NULL COMMENT '性别',
    `birthday` date        DEFAULT NULL COMMENT '出生年月',
    PRIMARY KEY (`id`),
    UNIQUE KEY `user_id_uindex` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user`
    DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `address`, `sex`, `birthday`)
VALUES (1, '张三', '武汉', 1, '2022-05-06'),
       (2, '李四', '上海', 1, '2022-05-12'),
       (3, '王五', '北京', 0, '2022-05-17');
/*!40000 ALTER TABLE `user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role`
(
    `user_id` int(30) NOT NULL,
    `role_id` int(30) NOT NULL,
    KEY `user_role_role_roleId_fk` (`role_id`),
    KEY `user_role_user_id_fk` (`user_id`),
    CONSTRAINT `user_role_role_roleId_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`roleId`),
    CONSTRAINT `user_role_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role`
    DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`, `role_id`)
VALUES (1, 1),
       (1, 2),
       (2, 1);
/*!40000 ALTER TABLE `user_role`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2022-05-06 21:47:37
