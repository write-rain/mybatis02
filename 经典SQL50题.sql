-- 1.查询" 01 “课程比” 02 "课程成绩高的学生的信息及课程分数
use sql_practice;
select *
from score
where c_id = 01;
select *
from score
where c_id = 02;
select st.*, sc1.s_score, sc2.s_score
from student st
         inner join (select * from score where c_id = 01) sc1 on st.s_id = sc1.s_id
         inner join (select * from score where c_id = 02) sc2 on sc1.s_id = sc2.s_id
where sc1.s_score > sc2.s_score;
--   查询同时存在" 01 “课程和” 02 "课程的情况
select *
from score sc1
where c_id = 01;
select *
from score sc2
where c_id = 02;
select st.*, sc1.s_score, sc2.s_score
from student st
         inner join (select *from score sc1 where c_id = 01) sc1 on st.s_id = sc1.s_id
         inner join (select * from score where c_id != 02) sc2 on sc1.s_id = sc2.s_id
where sc1.s_id = sc2.s_id;
--   查询存在" 01 “课程但可能不存在” 02 "课程的情况(不存在时显示为 null )
select *
from (select s_id, s_score ss1 from score where c_id = 01) sc1
         left join (select s_id, s_score ss2 from score where c_id = 02) sc2 on sc1.s_id = sc2.s_id;
--   查询不存在" 01 “课程但存在” 02 "课程的情况
select *
from (select s_id, s_score ss1 from score where c_id = 01) sc1
         right join (select s_id, s_score ss2 from score where c_id = 02) sc2 on sc1.s_id = sc2.s_id;
-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
select *
from score sc
where sc.s_score >= 60;
select st.s_id, st.s_name, round(avg(s_score), 2)
from student st,
     score sc
where st.s_id = sc.s_id
group by sc.s_id
having avg(s_score) >= 60;
-- 3.查询在 SC 表存在成绩的学生信息
select *
from student st
where st.s_id in (select distinct s_id from score);
-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select count(sc.c_id), sum(sc.s_score)
from score sc
group by sc.s_id;
select st.s_id, st.s_name, sc.count_sc, sc.sum_score
from student st
         inner join (select sc.s_id, count(sc.c_id) count_sc, sum(sc.s_score) sum_score
                     from score sc
                     group by sc.s_id) sc on st.s_id = sc.s_id;
--   查有成绩的学生信息
select *
from student st
where s_id in (select distinct sc.s_id from score sc);
-- 5.查询「李」姓老师的数量
-- 6.查询学过「张三」老师授课的同学的信息
select st.*
from student st,
     teacher t,
     course c,
     score s
where t.t_id = c.t_id
  and st.s_id = s.s_id
  and s.c_id = c.c_id
  and t.t_name = "张三";
-- 7.查询没有学全所有课程的同学的信息
select *
from student st
where st.s_id in (select s_id from score group by s_id having count(s_id) < 3);
select *
from student s
where s.s_id in (
    select sc.s_id from score sc group by sc.s_id having count(sc.c_id) < (select count(*) from course)
);

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
select st.*
from student st
where st.s_id in
      (select sc.s_id from score sc where st.s_id = sc.s_id and sc.c_id in (select c_id from score where s_id = 01));

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
select group_concat(sc.c_id)
from score sc
where sc.s_id = 01
group by sc.s_id;
select sc.s_id
from score sc
group by sc.s_id
having group_concat(sc.c_id) = (select group_concat(sc.c_id)
                                from score sc
                                where sc.s_id = 01
                                group by sc.s_id);
-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
select c_id
from course c,
     teacher t
where c.t_id = t.t_id
  and t.t_name = '张三';


select distinct st.s_name
from student st
where st.s_id not in (select sc.s_id
                      from score sc
                      where sc.c_id in (select c_id
                                        from course c,
                                             teacher t
                                        where c.t_id = t.t_id
                                          and t.t_name = '张三'
                      ));
-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
select s_id
from score sc
where sc.s_score < 60
group by sc.s_id
having count(sc.s_score) >= 2;
select st.s_id, st.s_name, round(AVG(sc.s_score), 0)
from student st,
     score sc
where st.s_id in (select s_id
                  from score sc
                  where sc.s_score < 60
                  group by sc.s_id
                  having count(sc.s_score) >= 2)
group by st.s_id;
-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
select sc.s_id
from score sc
where sc.c_id = 01
  and sc.s_score < 60;

select *
from student st,
     score sc
where st.s_id = sc.c_id = 01
  and sc.s_score < 60
order by sc.s_score desc;

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select sc.s_id                   "学号"
     , round(avg(sc.s_score), 2) 平均成绩
     , sum(sc.s_score)           "总成绩"
from score sc
group by sc.s_id
order by avg(sc.s_score) desc;

-- 14.查询各科成绩最高分、最低分和平均分：
--    以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
--    及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
--    要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列
select sc.c_id                                                                                      "课程ID",
       c.c_name                                                                                     "课程name",
       count(sc.s_id)                                                                               "选修人数",
       max(sc.s_score)                                                                              "最高分",
       min(sc.s_score)                                                                              "最低分",
       avg(sc.s_score)                                                                              "平均分",
       (sum(if(sc.s_score >= 60, 1, 0)) / (select count(*) from student)) * 100                     "及格率",
       (sum(if(sc.s_score >= 70 and sc.s_score < 80, 1, 0)) / (select count(*) from student)) * 100 "中等率",
       (sum(if(sc.s_score >= 80 and sc.s_score < 90, 1, 0)) / (select count(*) from student)) * 100 "优良率",
       (sum(if(sc.s_score >= 90, 1, 0)) / (select count(*) from student)) * 100                     "优秀率"
from score sc,
     course c
where sc.c_id = c.c_id
group by sc.c_id, c_name
order by count(sc.s_id) desc, sc.c_id asc;

select score.c_id                                                                             "课程 ID",
       c_name                                                                                 "课程 name",
       count(s_id)                                                                            "学科选修总人数",
       max(score.s_score)                                                                     "最高分",
       min(score.s_score)                                                                     "最低分",
       avg(score.s_score)                                                                     "平均分",
       (sum(if(s_score >= 60, 1, 0)) / (select count(*) from student)) * 100                  "及格率",
       (sum(if(s_score >= 70 and s_score < 80, 1, 0)) / (select count(*) from student)) * 100 "中等率",
       (sum(if(s_score >= 80 and s_score < 90, 1, 0)) / (select count(*) from student)) * 100 "优良率",
       (sum(if(s_score >= 90, 1, 0)) / (select count(*) from student)) * 100                  "优秀率"
from score,
     course
where score.c_id = course.c_id
group by score.c_id, c_name
order by count(s_id) desc, score.c_id asc;
-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
--    按各科成绩进行排序，并显示排名， Score 重复时合并名次
-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
--    查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
select(sum(if(score.s_score >= 0 and score.s_score < 60, 1, 0)) / count(s_id) * 100)
          "[60-0] 所占百分比",
      (sum(if(score.s_score >= 60 and score.s_score < 70, 1, 0)) / count(s_id) * 100)
          "[70-60] 所占百分比",
      (sum(if(score.s_score >= 70 and score.s_score < 85, 1, 0)) / count(s_id) * 100)
          "[85-70] 所占百分比",
      (sum(if(score.s_score >= 85 and score.s_score <= 100, 1, 0)) / count(s_id) * 100)
          "[100-85] 所占百分比"
from score
group by c_id;
-- 18.查询各科成绩前三名的记录
select *
from score sc
where c_id = 01
order by s_score desc
limit 3;


-- 19.查询每门课程被选修的学生数
select score.c_id, count(s_id)
from score
group by c_id;
-- 20.查询出只选修两门课程的学生学号和姓名
select st.s_id '姓名', st.s_name '学号', count(sc.c_id) '选修课程数'
from score sc,
     student st
where st.s_id = sc.s_id
group by st.s_id, st.s_name
having count(sc.c_id) = 2;
-- 21.查询男生、女生人数
select s_sex, count(s_id)
from student
group by s_sex;
-- 22.查询名字中含有「风」字的学生信息
select *
from student
where s_name like '%风%';
-- 23.查询同名同性学生名单，并统计同名人数
select s1.s_name, count(s1.s_id)
from student s1,
     student s2
where s1.s_name = s2.s_name
  and s1.s_sex = s2.s_sex
group by s1.s_name;
-- 24.查询 1990 年出生的学生名单
select s_name, s_birth
from student
where s_birth like '%1990%';

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
select c_id, round(avg(s_score), 2)
from score
group by c_id
order by avg(s_score) desc, c_id desc;
-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select st.s_id, st.s_name, round(avg(s_score), 2)
from student st,
     score sc
where st.s_id = sc.s_id
group by st.s_id
having avg(s_score) >= 85;
-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select st.s_name, sc.s_score
from student st,
     score sc,
     course c
where st.s_id = sc.s_id
  and sc.c_id = c.c_id
  and c.c_name = '数学'
  and sc.s_score < 60;
-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select st.s_name, sc.s_score, sc.c_id
from student st
         left join score sc on st.s_id = sc.s_id;
-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

select st.s_name, c.c_name, sc.s_score
from score sc,
     student st,
     course c
where st.s_id = sc.s_id
  and sc.c_id = c.c_id
  and st.s_id in (select sc.s_id from score sc where sc.s_score >= 70);

-- 30.查询不及格的课程
select c.c_name, sc.s_score
from score sc,
     course c
where sc.c_id = c.c_id;
-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
select st.s_name, st.s_id, sc.s_score
from score sc,
     student st
where st.s_id = sc.s_id
  and sc.c_id = 01
having sc.s_score >= 80;
-- 32.求每门课程的学生人数
select sc.c_id, count(sc.s_id)
from score sc
group by sc.c_id;

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
select st.s_id, st.s_name, sc.s_score
from student st,
     score sc,
     course c,
     teacher t
where t.t_name = '张三'
  and st.s_id = sc.s_id
  and sc.c_id = c.c_id
  and c.t_id = t.t_id
order by sc.s_score desc
limit 0,1;

-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩


-- 36.查询每门功成绩最好的前两名
-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。
-- 38.检索至少选修两门课程的学生学号
-- 39.查询选修了全部课程的学生信息
-- 40.查询各学生的年龄，只按年份来算
-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
-- 42.查询本周过生日的学生
-- 43.查询下周过生日的学生
-- 44.查询本月过生日的学生
-- 45.查询下月过生日的学生